package com.app.nassimhammadi.myfirstapplication;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private int ADD;
    private ArrayList<Etudiant> etudiants = new ArrayList<Etudiant>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void sendMessage(View view) {
        Intent add = new Intent(MainActivity.this, AjouterActivity.class );
        startActivityForResult(add,ADD);
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data) {
        if(data.hasExtra("etudiant")){
            Etudiant e = data.getParcelableExtra("etudiant");

            this.etudiants.add(e);
        }
    }


    public void showStudent(View view) {
        Intent show = new Intent(MainActivity.this, ShowActivity.class );
        show.putExtra("etudiants",this.etudiants);
        startActivity(show);
    }
}
