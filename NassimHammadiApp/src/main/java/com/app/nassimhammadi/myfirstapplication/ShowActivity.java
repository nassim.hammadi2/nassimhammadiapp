package com.app.nassimhammadi.myfirstapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ShowActivity extends AppCompatActivity {

    ArrayList<Etudiant> etudiants = new ArrayList<Etudiant>();
    ListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        Intent data = getIntent();
        if(data.hasExtra("etudiants")){
            this.etudiants = data.getParcelableArrayListExtra("etudiants");
        }
        mListView = (ListView) findViewById(R.id.listView);
        final ArrayAdapter<Etudiant> adapter = new ArrayAdapter<Etudiant>(ShowActivity.this,
                android.R.layout.simple_list_item_1, etudiants);
        mListView.setAdapter(adapter);
    }
}
