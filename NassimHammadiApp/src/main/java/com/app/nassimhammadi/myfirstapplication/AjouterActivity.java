package com.app.nassimhammadi.myfirstapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.ToggleButton;

public class AjouterActivity extends AppCompatActivity {


    private String redoublant;
    private String sexe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouter);
        EditText edittext= (EditText) findViewById(R.id.birthDate);
        setDate fromDate = new setDate(edittext, this);
    }


    public void ajouterEtudiant(View view) {
        EditText n = findViewById(R.id.nom);
        EditText p = findViewById(R.id.prenom);

        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.group);
        int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
        if (checkedRadioButtonId == -1) {
            // No item selected
        }
        else{
            if (checkedRadioButtonId == R.id.radio1) {
                sexe = "Homme";
            }
            else if (checkedRadioButtonId == R.id.radio2) {
                sexe = "femme";
            }
        }
        EditText e = findViewById(R.id.email);
        EditText naiss = findViewById(R.id.age);
        Spinner spinner = (Spinner) findViewById(R.id.options_spinner);
        String groupe = spinner.getSelectedItem().toString();

        ToggleButton toggle = (ToggleButton) findViewById(R.id.togglebutton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    redoublant = "redoublant";
                } else {
                    redoublant = "non redoublant";
                }
            }
        });

        String nom = n.getText().toString() ;
        String prenom= p.getText().toString();
        String email = e.getText().toString();


        Etudiant etudiant = new Etudiant(nom, prenom,sexe, email,"tttt", groupe,redoublant);
        Intent i = new Intent(AjouterActivity.this, MainActivity.class);
        i.putExtra("etudiant",etudiant);
        setResult(RESULT_OK,i);
        finish();

    }
}
