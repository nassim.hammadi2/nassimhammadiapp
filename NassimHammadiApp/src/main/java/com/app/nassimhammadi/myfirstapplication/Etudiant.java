package com.app.nassimhammadi.myfirstapplication;

import android.os.Parcel;
import android.os.Parcelable;

public class Etudiant implements Parcelable {

    private String nom;
    private String prenom;
    private String sexe;
    private String email;
    private String naissance;
    private String groupe;
    private String redoublant;

    public Etudiant(String nom, String prenom, String sexe, String email, String naissance, String groupe, String redoublant) {
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.email = email;
        this.naissance = naissance;
        this.groupe = groupe;
        this.redoublant = redoublant;
    }

    protected Etudiant(Parcel in) {
        nom = in.readString();
        prenom = in.readString();
        sexe = in.readString();
        email = in.readString();
        naissance = in.readString();
        groupe = in.readString();
        redoublant = in.readString();
    }

    public static final Creator<Etudiant> CREATOR = new Creator<Etudiant>() {
        @Override
        public Etudiant createFromParcel(Parcel in) {
            return new Etudiant(in);
        }

        @Override
        public Etudiant[] newArray(int size) {
            return new Etudiant[size];
        }
    };

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNaissance() {
        return naissance;
    }

    public void setNaissance(String naissance) {
        this.naissance = naissance;
    }

    public String getGroupe() {
        return groupe;
    }

    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    public String getRedoublant() {
        return redoublant;
    }

    public void setRedoublant(String redoublant) {
        this.redoublant = redoublant;
    }

    @Override
    public String toString() {
        return "Etudiant{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", sexe='" + sexe + '\'' +
                ", email='" + email + '\'' +
                ", naissance='" + naissance + '\'' +
                ", groupe='" + groupe + '\'' +
                ", redoublant='" + redoublant + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeString(prenom);
        dest.writeString(sexe);
        dest.writeString(email);
        dest.writeString(naissance);
        dest.writeString(groupe);
        dest.writeString(redoublant);
    }
}
